import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {HouseListComponent} from './houses/house-list/house-list.component';
import {HouseDetailComponent} from './houses/house-detail/house-detail.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    },
    {
        path: 'home',
        component: HomeComponent,
    },
    {
        path: 'houses',
        component: HouseListComponent,
        children: []
    },
    {
        path: 'houses/:id',
        component: HouseDetailComponent,
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
