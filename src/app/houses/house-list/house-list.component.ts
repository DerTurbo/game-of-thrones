import {Component, OnInit} from '@angular/core';
import {HousesService} from '../shared/houses.service';
import {House} from '../shared/house.model';

@Component({
    selector: 'app-house-list',
    templateUrl: './house-list.component.html',
    styleUrls: ['./house-list.component.scss']
})
export class HouseListComponent implements OnInit {
    public houses: House[] = [];
    public housesLoaded = false;

    constructor(private housesService: HousesService) {
    }

    ngOnInit(): void {
        this.getHouses();
    }

    private getHouses() {
        this.housesLoaded = false;
        this.housesService.getHouses().subscribe((houses: House[]) => {
            this.housesLoaded = true;

            if (!houses?.length) {
                return;
            }

            this.houses = houses;
        });
    }
}
