import {Component, OnInit} from '@angular/core';
import {HousesService} from '../shared/houses.service';
import {ActivatedRoute} from '@angular/router';
import {House} from '../shared/house.model';

@Component({
    selector: 'app-house-detail',
    templateUrl: './house-detail.component.html',
    styleUrls: ['./house-detail.component.scss']
})
export class HouseDetailComponent implements OnInit {
    public house: House;
    public id: number;
    public houseLoaded = false;

    constructor(private housesService: HousesService,
                private route: ActivatedRoute) {
        this.id = +this.route.snapshot.params?.id;
    }

    ngOnInit(): void {
        this.getHouseById();
    }

    private getHouseById() {
        if (!this.id) {
            this.houseLoaded = false;
            return;
        }

        this.housesService.getHouse(this.id).subscribe((house: House) => {
            this.houseLoaded = true;
            this.house = house;

        }, () => this.houseLoaded = true);
    }
}
