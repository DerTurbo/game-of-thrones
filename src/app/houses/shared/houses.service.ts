import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';
import {House} from './house.model';

@Injectable({
    providedIn: 'root'
})
export class HousesService {
    constructor(private http: HttpClient) {
    }

    public getHouse(id: number) {
        return this.http.get(environment.gotAPI + '/houses/' + id).pipe(
            map((house: House) => {
                // Remove empty titles strings from array
                house.titles = house.titles.filter(t => t);
                // Remove empty seat strings from array
                house.seats = house.seats.filter(s => s);
                // Remove empty ancestral weapons strings from array
                house.ancestralWeapons = house.ancestralWeapons.filter(w => w);

                return house;
            })
        );
    }

    public getHouses() {
        const apiUrl = environment.gotAPI + '/houses';
        return this.http.get(apiUrl)
            .pipe(
                map((houses: House[]) => {
                    const resultArray = [];
                    houses.forEach(item => {
                        // Extract id from url param
                        item.id = +item.url.replace(apiUrl + '/', '');
                        // Remove empty titles strings from array
                        item.titles = item.titles.filter(t => t);
                        // Remove empty seat strings from array
                        item.seats = item.seats.filter(s => s);

                        resultArray.push(item);
                    });

                    return resultArray;
                })
            );
    }
}
