export interface House {
    id: number;
    name: string;
    region: string;
    url: string;
    seats: string[];
    words: string;
    titles: string[];
    coatOfArms: string;
    founded: string;
    diedOut: string;
    ancestralWeapons: string[];
}
