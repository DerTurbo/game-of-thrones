import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HouseListComponent} from './houses/house-list/house-list.component';
import {MainNavComponent} from './layout/main-nav/main-nav.component';
import {HomeComponent} from './home/home.component';
import {HttpClientModule} from '@angular/common/http';
import {HouseDetailComponent} from './houses/house-detail/house-detail.component';

@NgModule({
    declarations: [
        AppComponent,
        HouseListComponent,
        MainNavComponent,
        HomeComponent,
        HouseDetailComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
